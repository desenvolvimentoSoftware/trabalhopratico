package Negocio;

import java.util.ArrayList;

import persistencia.ClasseBasico;

public class ListaClasseBasico {
	protected ArrayList<ClasseBasico> classes;
	
	public ListaClasseBasico() {
		classes = new ArrayList<ClasseBasico>();
	}
	
	public ListaClasseBasico(ClasseBasico ct) {
		classes = new ArrayList<ClasseBasico>();
		classes.add(ct);
	}
	public ListaClasseBasico(ArrayList<ClasseBasico> classes) {
		this.classes = classes;
	}
	
	public int adicionarClasseBasico(int nivel_id) throws Exception {
		ClasseBasico ct = new ClasseBasico(nivel_id);
		classes.add(ct);
		
		return Integer.parseInt(ct.get("id"));
	}
	
	public int adicionarClasseBasico(ClasseBasico ct) throws Exception {
		classes.add(ct);		
		return Integer.parseInt(ct.get("id"));
	}
	
	public int adicionarClasseBasico(int nivelEnsino, String nome, String ano_atual) throws Exception {
		ClasseBasico ct = new ClasseBasico(nivelEnsino, nome, ano_atual);
		classes.add(ct);
		
		return Integer.parseInt(ct.get("id"));
	}
	
	public boolean adicionarAtributoClasseBasico(String nome, String tipo) throws Exception {
		if (classes.size() > 0) {
			classes.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < classes.size(); i ++) {
				classes.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	public boolean removerAtributoClasseBasico(String nome) throws Exception {
		if (classes.size() > 0) {
			classes.get(0).deleteAtributo(nome);
			for (int i = 1; i < classes.size(); i ++) {
				classes.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarClasseBasico(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				return(classes.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarClasseBasico(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				return(classes.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerClasseBasico(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				classes.get(i).delete();
				classes.remove(classes.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerClasseBasico(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				return(classes.get(i).delete());
			}
		}
		return false;
	}
	
	public ClasseBasico getClasseBasico(String value) {
		for (int i = 0; i < this.classes.size(); i++) {
			if (classes.get(i).contains(value)) {
				return classes.get(i);
			}	
		}
		return null;
	}
	
	public int getSize() {
		return classes.size();
	}
	
	public ArrayList<ClasseBasico> getDisciplinas() {
		return classes;
	}
	
	public void setClasseBasico(ArrayList<ClasseBasico> classes) {
		this.classes = classes;
	}

	@Override
	public String toString() {
		return "listaClasseBasico [classes=" + classes + "]";
	}
	
}
