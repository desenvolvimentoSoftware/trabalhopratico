package Negocio;

import java.util.ArrayList;

import persistencia.DisciplinaSupTurma;

public class ListaDisciplinaSupTurma {
	private ArrayList<DisciplinaSupTurma> disciplinas_turmas;
	
	public ListaDisciplinaSupTurma() {
		disciplinas_turmas = new ArrayList<DisciplinaSupTurma>();
	}
	
	public ListaDisciplinaSupTurma(DisciplinaSupTurma dt) {
		disciplinas_turmas = new ArrayList<DisciplinaSupTurma>();
		disciplinas_turmas.add(dt);
	}
	public ListaDisciplinaSupTurma(ArrayList<DisciplinaSupTurma> disciplinas) {
		this.disciplinas_turmas = disciplinas;
	}
	
	public int adicionarDisciplinaTurma(int nivel_id, int disciplina, int turma) throws Exception {
		DisciplinaSupTurma dt = new DisciplinaSupTurma(nivel_id, disciplina, turma);
		disciplinas_turmas.add(dt);
		
		return Integer.parseInt(dt.get("id"));
	}	
	
	public int adicionarDisciplinaTurma(DisciplinaSupTurma dt) throws Exception {
		disciplinas_turmas.add(dt);
		
		return Integer.parseInt(dt.get("id"));
	}	
	
	public ArrayList<Integer> getTurmasFromDisciplinaTurma(int disciplina) {
		ArrayList<Integer> turmas = new ArrayList<Integer>();
		String disc = "" + disciplina;
		for (int i = 0; i < disciplinas_turmas.size(); i ++) {
			if (disciplinas_turmas.get(i).contains(disc)) {
				turmas.add(Integer.parseInt(disciplinas_turmas.get(i).get(disc)));
			}	
		}
		return turmas;
	}
	
	public int getSize() {
		return disciplinas_turmas.size();
	}
	
	public ArrayList<DisciplinaSupTurma> getDiscplinaTurma() {
		return disciplinas_turmas;
	}
	
	public void setDiscplinaTurma(ArrayList<DisciplinaSupTurma> disciplinas_turmas) {
		this.disciplinas_turmas = disciplinas_turmas;
	}
	
	public boolean removerDisciplinaTurma(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < disciplinas_turmas.size(); i ++) {
			if(Integer.parseInt(disciplinas_turmas.get(i).get("id")) == id ) {
				disciplinas_turmas.get(i).delete();
				disciplinas_turmas.remove(disciplinas_turmas.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerDisciplinaTurma(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < disciplinas_turmas.size(); i ++) {
			if(Integer.parseInt(disciplinas_turmas.get(i).get("id")) == id ) {
				return(disciplinas_turmas.get(i).delete());
			}
		}
		return false;
	}
	
	public boolean editarDisciplinaTurma(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < disciplinas_turmas.size(); i ++) {
			if(Integer.parseInt(disciplinas_turmas.get(i).get("id")) == id ) {
				return(disciplinas_turmas.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
}
