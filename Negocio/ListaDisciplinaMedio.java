package Negocio;

import java.util.ArrayList;

import persistencia.Disciplina;
import persistencia.DisciplinaMedio;

public class ListaDisciplinaMedio {
	private ArrayList<DisciplinaMedio> disciplinas;
	
	public ListaDisciplinaMedio() {
		disciplinas = new ArrayList<DisciplinaMedio>();
	}
	
	public ListaDisciplinaMedio(DisciplinaMedio t) {
		disciplinas = new ArrayList<DisciplinaMedio>();
		disciplinas.add(t);
	}
	public ListaDisciplinaMedio(ArrayList<DisciplinaMedio> disciplinas) {
		this.disciplinas = disciplinas;
	}
	
	public int adicionarDisciplina(int nivel_id) throws Exception {
		DisciplinaMedio d = new DisciplinaMedio(nivel_id);
		disciplinas.add(d);
		
		return Integer.parseInt(d.get("id"));
	}
	
	public int adicionarDisciplina(DisciplinaMedio d) throws Exception {
		disciplinas.add(d);		
		return Integer.parseInt(d.get("id"));
	}
	
	public int adicionarDisciplina(int nivel_id, String nome, int ch_teorica, int ch_pratica, int ch_total) throws Exception {
		DisciplinaMedio d = new DisciplinaMedio(nivel_id, nome, ch_total);
		disciplinas.add(d);
		
		return Integer.parseInt(d.get("id"));
	}
	
	public boolean adicionarAtributoDisciplina(String nome, String tipo) throws Exception {
		if (disciplinas.size() > 0) {
			disciplinas.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < disciplinas.size(); i ++) {
				disciplinas.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	public boolean removerAtributoDisciplina(String nome) throws Exception {
		if (disciplinas.size() > 0) {
			disciplinas.get(0).deleteAtributo(nome);
			for (int i = 1; i < disciplinas.size(); i ++) {
				disciplinas.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarDisciplina(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < disciplinas.size(); i ++) {
			if(Integer.parseInt(disciplinas.get(i).get("id")) == id ) {
				return(disciplinas.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarDisciplina(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < disciplinas.size(); i ++) {
			if(Integer.parseInt(disciplinas.get(i).get("id")) == id ) {
				return(disciplinas.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerDisciplina(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < disciplinas.size(); i ++) {
			if(Integer.parseInt(disciplinas.get(i).get("id")) == id ) {
				disciplinas.get(i).delete();
				disciplinas.remove(disciplinas.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerDisciplina(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < disciplinas.size(); i ++) {
			if(Integer.parseInt(disciplinas.get(i).get("id")) == id ) {
				return(disciplinas.get(i).delete());
			}
		}
		return false;
	}
	
	public Disciplina getDisciplina(String value) {
		for (int i = 0; i < this.disciplinas.size(); i++) {
			if (disciplinas.get(i).contains(value)) {
				return disciplinas.get(i);
			}	
		}
		return null;
	}
	
	public int getSize() {
		return disciplinas.size();
	}
	
	public ArrayList<DisciplinaMedio> getDisciplinas() {
		return disciplinas;
	}
	
	public void setDisciplinas(ArrayList<DisciplinaMedio> disciplinas) {
		this.disciplinas = disciplinas;
	}

	@Override
	public String toString() {
		return "ListaDisciplinaSup [disciplinas=" + disciplinas + "]";
	}
}
