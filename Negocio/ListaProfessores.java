package Negocio;

import java.util.ArrayList;

import persistencia.Professor;

public class ListaProfessores {
	private ArrayList<Professor> professores;
	
	public ListaProfessores() {
		professores = new ArrayList<Professor>();
	}
	
	public ListaProfessores(Professor p) {
		professores = new ArrayList<Professor>();
		professores.add(p);
	}
	public ListaProfessores(ArrayList<Professor> disciplinas) {
		this.professores = disciplinas;
	}
	
	public int adicionarProfessor(int nivel_id) throws Exception {
		Professor p = new Professor(nivel_id);
		professores.add(p);
		
		return Integer.parseInt(p.get("id"));
	}
	
	public int adicionarProfessor(int nivel_id, String nome, String email, int ch_minima, int ch_maxima) throws Exception {
		Professor p = new Professor(nivel_id, nome, email, ch_minima, ch_maxima);
		professores.add(p);
		
		return Integer.parseInt(p.get("id"));
	}
	
	public boolean adicionarAtributoProfessor(String nome, String tipo) throws Exception {
		if (professores.size() > 0) {
			professores.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < professores.size(); i ++) {
				professores.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	public boolean removerAtributoProfessor(String nome) throws Exception {
		if (professores.size() > 0) {
			professores.get(0).deleteAtributo(nome);
			for (int i = 1; i < professores.size(); i ++) {
				professores.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarProfessor(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < professores.size(); i ++) {
			if(Integer.parseInt(professores.get(i).get("id")) == id ) {
				return(professores.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarProfessor(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < professores.size(); i ++) {
			if(Integer.parseInt(professores.get(i).get("id")) == id ) {
				return(professores.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerProfessor(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < professores.size(); i ++) {
			if(Integer.parseInt(professores.get(i).get("id")) == id ) {
				professores.get(i).delete();
				professores.remove(professores.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerProfessor(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < professores.size(); i ++) {
			if(Integer.parseInt(professores.get(i).get("id")) == id ) {
				return(professores.get(i).delete());
			}
		}
		return false;
	}
	
	public Professor getProfessor(String value) {
		for (int i = 0; i < this.professores.size(); i++) {
			if (professores.get(i).contains(value)) {
				return professores.get(i);
			}	
		}
		return null;
	}
	
	public int adicionarProfessor(Professor p) throws Exception {
		professores.add(p);		
		return Integer.parseInt(p.get("id"));
	}
	
	public int getSize() {
		return professores.size();
	}
	
	public ArrayList<Professor> getProfessores() {
		return professores;
	}
	
	public void setProfessores(ArrayList<Professor> professores) {
		this.professores = professores;
	}

	@Override
	public String toString() {
		return "ListaProfessores [professores=" + professores + "]";
	}
	
	public Professor get(int i) {
		return professores.get(i);
	}
	
	
}
