package Negocio;

import java.util.ArrayList;

import persistencia.Conexao;
import persistencia.CursoSuperior;
import persistencia.CursoSuperiorTurma;
import persistencia.Disciplina;
import persistencia.DisciplinaSup;
import persistencia.DisciplinaSupTurma;
import persistencia.PesoTurma;
import persistencia.Professor;
import persistencia.SuperiorPersistencia;
import persistencia.Turma;

public class Superior extends SuperiorPersistencia{
		
	public Superior(ArrayList<DisciplinaSup> disciplinas, ArrayList<CursoSuperior> cursos,
			ArrayList<Turma> turmas, ArrayList<Professor> professores, ArrayList<PesoTurma> pesos) throws Exception {
		this.prepareDatabase();
		this.id = Conexao.criarNivelEnsino("superior");
		this.disciplinas = new ListaDisciplinaSup(disciplinas);
		this.cursos = new ListaCursos(cursos);
		this.turmas = new ListaTurmas(turmas);
		this.professores = new ListaProfessores(professores);
		this.pesos = new ListaPesosTurmas(pesos);
		this.recuperaDatabase();

	}
	
	public Superior(ArrayList<Professor> professores) throws Exception {
		this.prepareDatabase();
		this.id = Conexao.criarNivelEnsino("superior");
		this.disciplinas = new ListaDisciplinaSup();
		this.cursos = new ListaCursos();
		this.turmas = new ListaTurmas();
		this.professores = new ListaProfessores(professores);
		this.pesos = new ListaPesosTurmas();
		this.recuperaDatabase();

	}
	
	public Superior(ArrayList<Professor> professores, ArrayList<CursoSuperior> cursos) throws Exception {
		this.prepareDatabase();
		this.id = Conexao.criarNivelEnsino("superior");
		this.disciplinas = new ListaDisciplinaSup();
		this.cursos = new ListaCursos(cursos);
		this.turmas = new ListaTurmas();
		this.professores = new ListaProfessores(professores);
		this.pesos = new ListaPesosTurmas();
		this.recuperaDatabase();

	}
	
	public Superior(ArrayList<Professor> professores, ArrayList<CursoSuperior> cursos, ArrayList<DisciplinaSup> disciplinas) throws Exception {
		this.prepareDatabase();
		this.id = Conexao.criarNivelEnsino("superior");
		this.disciplinas = new ListaDisciplinaSup(disciplinas);
		this.cursos = new ListaCursos(cursos);
		this.turmas = new ListaTurmas();
		this.professores = new ListaProfessores(professores);
		this.pesos = new ListaPesosTurmas();
		this.recuperaDatabase();

	}
	
	public Superior(ArrayList<Professor> professores, ArrayList<CursoSuperior> cursos, ArrayList<DisciplinaSup> disciplinas, ArrayList<Turma> turmas) throws Exception {
		this.prepareDatabase();
		this.id = Conexao.criarNivelEnsino("superior");
		this.disciplinas = new ListaDisciplinaSup(disciplinas);
		this.cursos = new ListaCursos(cursos);
		this.turmas = new ListaTurmas(turmas);
		this.professores = new ListaProfessores(professores);
		this.pesos = new ListaPesosTurmas();
		this.recuperaDatabase();

	}
	
	public Superior() throws Exception {
		this.prepareDatabase();		
		this.id = Conexao.criarNivelEnsino("superior");
		this.disciplinas = new ListaDisciplinaSup();
		this.cursos = new ListaCursos();
		this.turmas = new ListaTurmas();
		this.professores = new ListaProfessores();
		this.pesos = new ListaPesosTurmas();
		this.recuperaDatabase();

	}
	
	@Override
	public boolean distribuir() {
		int j = 0;
		for (int i = 0; i < turmas.getSize(); i ++) {
			if (j > professores.getSize()) j = 0;
			System.out.println("Professor "+ professores.get(j).get("nome") + " fica com turma " + turmas.get(i).get("nome"));
		}
		return false;
	}
	
	//Turmas
	public int adicionarTurma(String nome, int semester) throws Exception {
		return turmas.adicionarTurma(this.id, nome, semester);
	}
	
	public boolean adicionarAtributoTurma(String nome, String tipo) throws Exception {
		return turmas.adicionarAtributoTurma(nome, tipo);
	}
	
	public boolean removerAtributoTurma(String nome) throws Exception {
		return turmas.removerAtributoTurma(nome);
	}
	
	public boolean editarTurma(String campo, String valor, String codigo) throws Exception {
		return turmas.editarTurma(campo, valor, codigo);
	}
	
	public boolean editarTurma(String campo, String valor, int codigo) throws Exception {
		return turmas.editarTurma(campo, valor, codigo);
	}
	
	public boolean removerTurma(int codigo) throws Exception {
		return turmas.removerTurma(codigo);
	}
	
	public boolean removerTurma(String codigo) throws Exception {
		return turmas.removerTurma(codigo);
	}
	
	public int getIdTurma(String value) {
		return Integer.parseInt(turmas.getTurma(value).get("id"));
	}
	
	public Turma getTurma(String value) {
		return turmas.getTurma(value);
	}
	
	
	
	
	//Disciplinas
	public int adicionarDisciplina(String nome, int ch_teorica, int ch_pratica, int ch_total) throws Exception {
		return disciplinas.adicionarDisciplina(this.id, nome, ch_teorica, ch_pratica, ch_total);
	}
	
	public boolean adicionarAtributoDisciplina(String nome, String tipo) throws Exception {
		return disciplinas.adicionarAtributoDisciplina(nome, tipo);
	}
	
	public boolean removerAtributoDisciplina(String nome) throws Exception {
		return disciplinas.removerAtributoDisciplina(nome);
	}
	
	public boolean editarDisciplina(String campo, String valor, String codigo) throws Exception {
		return disciplinas.editarDisciplina(campo, valor, codigo);
	}
	
	public boolean editarDisciplina(String campo, String valor, int codigo) throws Exception {
		return disciplinas.editarDisciplina(campo, valor, codigo);
	}
	
	public boolean removerDisciplina(int codigo) throws Exception {
		return disciplinas.removerDisciplina(codigo);
	}
	
	public boolean removerDisciplina(String codigo) throws Exception {
		return disciplinas.removerDisciplina(codigo);
	}
	
	public int getIdDisciplina(String value) {
		return Integer.parseInt(disciplinas.getDisciplina(value).get("id"));
	}
	
	public Disciplina getDisciplina(String value) {
		return disciplinas.getDisciplina(value);
	}
	
	
	
	
	//DisciplinaTurma
	public int adicionarDisciplinaTurma(int disciplina, int turma) throws Exception {
		return disciplinas_turmas.adicionarDisciplinaTurma(this.id, disciplina, turma);
	}	
	
	public int getDisciplinaTurmaSize() {
		return disciplinas_turmas.getSize();
	}
	
	public ArrayList<Integer> getTurmasFromDisciplinaTurma(int disciplina) {
		return disciplinas_turmas.getTurmasFromDisciplinaTurma(disciplina);
	}
	
	
	
	//CursoTurma
	public int adicionarCursoTurma(int curso, int turma) throws Exception {
		CursoSuperiorTurma ct = new CursoSuperiorTurma(this.id, curso, turma);
		cursos_turmas.add(ct);
		
		return Integer.parseInt(ct.get("id"));
	}
	
	
	//Professores
	public int adicionarProfessor() throws Exception {
		return professores.adicionarProfessor(this.id);
	}
	
	public int adicionarProfessor(String nome, String email, int ch_minima, int ch_maxima) throws Exception {
		return professores.adicionarProfessor(this.id, nome, email, ch_minima, ch_maxima);
	}
	
	public boolean adicionarAtributoProfessor(String nome, String tipo) throws Exception {
		return professores.adicionarAtributoProfessor(nome, tipo);
	}
	
	public boolean removerAtributoProfessor(String nome) throws Exception {
		return professores.removerAtributoProfessor(nome);
	}
		
	public boolean editarProfessor(String campo, String valor, String codigo) throws Exception {
		return professores.editarProfessor(campo, valor, codigo);
	}
	
	public boolean editarProfessor(String campo, String valor, int codigo) throws Exception {
		return professores.editarProfessor(campo, valor, codigo);
	}
	
	public boolean removerProfessor(int codigo) throws Exception {
		return professores.removerProfessor(codigo);
	}
	
	public boolean removerProfessor(String codigo) throws Exception {
		return professores.removerProfessor(codigo);
	}
	
	public int getIdProfessor(String value) {
		return Integer.parseInt(professores.getProfessor(value).get("id"));
	}
	
	public Professor getProfessor(String value) {
		return professores.getProfessor(value);
	}
	
	
	//cursos
		
	public int adicionarCurso() throws Exception {
		return cursos.adicionarCurso(this.id);
	}
	
	
	
	public int adicionarCurso(String nome, String acronimo, String descricao) throws Exception {
		return cursos.adicionarCurso(this.id, nome, acronimo, descricao);
	}
	
	public boolean adicionarAtributoCurso(String nome, String tipo) throws Exception {
		return cursos.adicionarAtributoCurso(nome, tipo);
	}
	
	public boolean removerAtributoCurso(String nome) throws Exception {
		return cursos.removerAtributoCurso(nome);
	}
	
	
	
	public boolean editarCurso(String campo, String valor, String codigo) throws Exception {
		return cursos.editarCurso(campo, valor, codigo);
	}
	
	public boolean editarCurso(String campo, String valor, int codigo) throws Exception {
		return cursos.editarCurso(campo, valor, codigo);
	}
	
	public boolean removerCurso(int codigo) throws Exception {
		return cursos.removerCurso(codigo);
	}
	
	public boolean removerCurso(String codigo) throws Exception {
		return cursos.removerCurso(codigo);
	}
	
	public int getIdCurso(String value) {
		return Integer.parseInt(cursos.getCurso(value).get("id"));
	}
	
	public CursoSuperior getCurso(String value) {
		return cursos.getCurso(value);
	}
	
	public int getProfessoresSize() {
		return professores.getSize();
	}
	
	public int getTurmasSize() {
		return turmas.getSize();
	}
	
	public int getDisciplinasSize() {
		return disciplinas.getSize();
	}
	
	public int getCursosSize() {
		return cursos.getSize();
	}
	
	
	//pesos
	public int adicionarPesoTurma(int professor, int peso, int turma) throws Exception {
		return pesos.adicionarPesoTurma(this.id, professor, peso, turma);
	}	
	
	public int adicionarPesoTurma(PesoTurma pt) throws Exception {
		return pesos.adicionarPesoTurma(pt);
	}	
	
	public int getSize() {
		return pesos.getSize();
	}
	
		
		
	public boolean removerPesoTurma(int codigo) throws Exception {
		return pesos.removerPesoTurma(codigo);
	}
	
	public boolean removerPesoTurma(String codigo) throws Exception {
		return pesos.removerPesoTurma(codigo);
	}
	
	public boolean editarPesoTurma(String campo, String valor, int codigo) throws Exception {
		return pesos.editarPesoTurma(campo, valor, codigo);
	}
	
	
	
	
	
	
	
	
	

}
