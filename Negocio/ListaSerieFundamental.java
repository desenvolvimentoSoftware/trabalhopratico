package Negocio;

import java.util.ArrayList;

import persistencia.SerieFundamental;

public class ListaSerieFundamental {
	private ArrayList<SerieFundamental> series;
	
	public ListaSerieFundamental() {
		series = new ArrayList<SerieFundamental>();
	}
	
	public ListaSerieFundamental(SerieFundamental c) {
		series = new ArrayList<SerieFundamental>();
		series.add(c);
	}
	public ListaSerieFundamental(ArrayList<SerieFundamental> series) {
		this.series = series;
	}
	
	public int adicionarSerie(int nivel_id) throws Exception {
		SerieFundamental c = new SerieFundamental(nivel_id);
		series.add(c);
		
		return Integer.parseInt(c.get("id"));
	}
	
	public int adicionarSerie(int nivel_id, String nome, String descricao) throws Exception {
		SerieFundamental c = new SerieFundamental(nivel_id, nome, descricao);
		series.add(c);
		
		return Integer.parseInt(c.get("id"));
	}
	
	public boolean adicionarAtributoSerie(String nome, String tipo) throws Exception {
		if (series.size() > 0) {
			series.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < series.size(); i ++) {
				series.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	public boolean removerAtributoSerie(String nome) throws Exception {
		if (series.size() > 0) {
			series.get(0).deleteAtributo(nome);
			for (int i = 1; i < series.size(); i ++) {
				series.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarSerie(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < series.size(); i ++) {
			if(Integer.parseInt(series.get(i).get("id")) == id ) {
				return(series.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarSerie(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < series.size(); i ++) {
			if(Integer.parseInt(series.get(i).get("id")) == id ) {
				return(series.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerSerie(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < series.size(); i ++) {
			if(Integer.parseInt(series.get(i).get("id")) == id ) {
				series.get(i).delete();
				series.remove(series.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerSerie(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < series.size(); i ++) {
			if(Integer.parseInt(series.get(i).get("id")) == id ) {
				return(series.get(i).delete());
			}
		}
		return false;
	}
	
	public SerieFundamental getSerie(String value) {
		for (int i = 0; i < this.series.size(); i++) {
			if (series.get(i).contains(value)) {
				return series.get(i);
			}	
		}
		return null;
	}
	
	public int adicionarSerie(SerieFundamental p) throws Exception {
		series.add(p);		
		return Integer.parseInt(p.get("id"));
	}
	
	public int getSize() {
		return series.size();
	}
	
	public ArrayList<SerieFundamental> getCursos() {
		return series;
	}
	
	public void setCursos(ArrayList<SerieFundamental> series) {
		this.series = series;
	}

	@Override
	public String toString() {
		return "Serie [series=" + series + "]";
	}
}
