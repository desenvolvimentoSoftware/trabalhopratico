package Negocio;

import java.util.ArrayList;

import persistencia.CursoSuperior;


public class ListaCursos {
	private ArrayList<CursoSuperior> cursos;
	
	public ListaCursos() {
		cursos = new ArrayList<CursoSuperior>();
	}
	
	public ListaCursos(CursoSuperior c) {
		cursos = new ArrayList<CursoSuperior>();
		cursos.add(c);
	}
	public ListaCursos(ArrayList<CursoSuperior> cursos) {
		this.cursos = cursos;
	}
	
	public int adicionarCurso(int nivel_id) throws Exception {
		CursoSuperior c = new CursoSuperior(nivel_id);
		cursos.add(c);
		
		return Integer.parseInt(c.get("id"));
	}
	
	public int adicionarCurso(int nivel_id, String nome, String acronimo, String descricao) throws Exception {
		CursoSuperior c = new CursoSuperior(nivel_id, nome, acronimo, descricao);
		cursos.add(c);
		
		return Integer.parseInt(c.get("id"));
	}
	
	public boolean adicionarAtributoCurso(String nome, String tipo) throws Exception {
		if (cursos.size() > 0) {
			cursos.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < cursos.size(); i ++) {
				cursos.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	public boolean removerAtributoCurso(String nome) throws Exception {
		if (cursos.size() > 0) {
			cursos.get(0).deleteAtributo(nome);
			for (int i = 1; i < cursos.size(); i ++) {
				cursos.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarCurso(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < cursos.size(); i ++) {
			if(Integer.parseInt(cursos.get(i).get("id")) == id ) {
				return(cursos.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarCurso(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < cursos.size(); i ++) {
			if(Integer.parseInt(cursos.get(i).get("id")) == id ) {
				return(cursos.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerCurso(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < cursos.size(); i ++) {
			if(Integer.parseInt(cursos.get(i).get("id")) == id ) {
				cursos.get(i).delete();
				cursos.remove(cursos.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerCurso(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < cursos.size(); i ++) {
			if(Integer.parseInt(cursos.get(i).get("id")) == id ) {
				return(cursos.get(i).delete());
			}
		}
		return false;
	}
	
	public CursoSuperior getCurso(String value) {
		for (int i = 0; i < this.cursos.size(); i++) {
			if (cursos.get(i).contains(value)) {
				return cursos.get(i);
			}	
		}
		return null;
	}
	
	public int adicionarCurso(CursoSuperior p) throws Exception {
		cursos.add(p);		
		return Integer.parseInt(p.get("id"));
	}
	
	public int getSize() {
		return cursos.size();
	}
	
	public ArrayList<CursoSuperior> getCursos() {
		return cursos;
	}
	
	public void setCursos(ArrayList<CursoSuperior> cursos) {
		this.cursos = cursos;
	}

	@Override
	public String toString() {
		return "Curso [cursos=" + cursos + "]";
	}
	
}
