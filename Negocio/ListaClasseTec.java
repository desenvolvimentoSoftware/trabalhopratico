package Negocio;

import java.util.ArrayList;

import persistencia.ClasseTec;

public class ListaClasseTec {
private ArrayList<ClasseTec> classes;
	
	public ListaClasseTec() {
		classes = new ArrayList<ClasseTec>();
	}
	
	public ListaClasseTec(ClasseTec ct) {
		classes = new ArrayList<ClasseTec>();
		classes.add(ct);
	}
	public ListaClasseTec(ArrayList<ClasseTec> classes) {
		this.classes = classes;
	}
	
	public int adicionarClasseTec(int nivel_id) throws Exception {
		ClasseTec ct = new ClasseTec(nivel_id);
		classes.add(ct);
		
		return Integer.parseInt(ct.get("id"));
	}
	
	public int adicionarClasseTec(ClasseTec ct) throws Exception {
		classes.add(ct);		
		return Integer.parseInt(ct.get("id"));
	}
	
	public int adicionarClasseTec(int nivelEnsino, String nome, int semestre, String dataInicio, String dataFim) throws Exception {
		ClasseTec ct = new ClasseTec(nivelEnsino, nome, semestre, dataInicio, dataFim);
		classes.add(ct);
		
		return Integer.parseInt(ct.get("id"));
	}
	
	public boolean adicionarAtributoClasseTec(String nome, String tipo) throws Exception {
		if (classes.size() > 0) {
			classes.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < classes.size(); i ++) {
				classes.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	public boolean removerAtributoClasseTec(String nome) throws Exception {
		if (classes.size() > 0) {
			classes.get(0).deleteAtributo(nome);
			for (int i = 1; i < classes.size(); i ++) {
				classes.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarClasseTec(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				return(classes.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarClasseTec(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				return(classes.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerClasseTec(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				classes.get(i).delete();
				classes.remove(classes.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerClasseTec(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < classes.size(); i ++) {
			if(Integer.parseInt(classes.get(i).get("id")) == id ) {
				return(classes.get(i).delete());
			}
		}
		return false;
	}
	
	public ClasseTec getClasseTec(String value) {
		for (int i = 0; i < this.classes.size(); i++) {
			if (classes.get(i).contains(value)) {
				return classes.get(i);
			}	
		}
		return null;
	}
	
	public int getSize() {
		return classes.size();
	}
	
	public ArrayList<ClasseTec> getDisciplinas() {
		return classes;
	}
	
	public void setClasseTec(ArrayList<ClasseTec> classes) {
		this.classes = classes;
	}

	@Override
	public String toString() {
		return "ListaClasseTec [classes=" + classes + "]";
	}
	
}
