package Negocio;

import java.util.ArrayList;

import persistencia.PesoTurma;
import persistencia.Professor;

public class ListaPesosTurmas {
private ArrayList<PesoTurma> pesos_turmas;
	
	public ListaPesosTurmas() {
		pesos_turmas = new ArrayList<PesoTurma>();
	}
	
	public ListaPesosTurmas(PesoTurma pt) {
		pesos_turmas = new ArrayList<PesoTurma>();
		pesos_turmas.add(pt);
	}
	
	public ListaPesosTurmas(ArrayList<PesoTurma> pesos_turmas) {
		this.pesos_turmas = pesos_turmas;
	}
	
	public int adicionarPesoTurma(int nivel_id, int professor, int peso, int turma) throws Exception {
		PesoTurma pt = new PesoTurma(nivel_id, professor, peso, turma);
		pesos_turmas.add(pt);
		
		return Integer.parseInt(pt.get("id"));
	}	
	
	public int adicionarPesoTurma(PesoTurma pt) throws Exception {
		pesos_turmas.add(pt);
		
		return Integer.parseInt(pt.get("id"));
	}	
	
	public int getSize() {
		return pesos_turmas.size();
	}
	
	public ArrayList<PesoTurma> getPesos(){
		return pesos_turmas;
	}
	
	public void setPesos(ArrayList<PesoTurma> pesos_turmas){
		this.pesos_turmas = pesos_turmas;
	}
	
	public boolean removerPesoTurma(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < pesos_turmas.size(); i ++) {
			if(Integer.parseInt(pesos_turmas.get(i).get("id")) == id ) {
				pesos_turmas.get(i).delete();
				pesos_turmas.remove(pesos_turmas.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerPesoTurma(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < pesos_turmas.size(); i ++) {
			if(Integer.parseInt(pesos_turmas.get(i).get("id")) == id ) {
				return(pesos_turmas.get(i).delete());
			}
		}
		return false;
	}
	
	public boolean editarPesoTurma(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < pesos_turmas.size(); i ++) {
			if(Integer.parseInt(pesos_turmas.get(i).get("id")) == id ) {
				return(pesos_turmas.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public PesoTurma get(int i) {
		return pesos_turmas.get(i);
	}
}
