package Negocio;

import java.util.ArrayList;

import persistencia.Professor;
import persistencia.Turma;

public class ListaTurmas {
	private ArrayList<Turma> turmas;
	
	public ListaTurmas() {
		turmas = new ArrayList<Turma>();
	}
	
	public ListaTurmas(Turma t) {
		turmas = new ArrayList<Turma>();
		turmas.add(t);
	}
	public ListaTurmas(ArrayList<Turma> turmas) {
		this.turmas = turmas;
	}
	
	
	public int adicionarTurma(int nivel_id, String nome, int semester) throws Exception {
		Turma t = new Turma(nivel_id, nome, semester);
		turmas.add(t);
		
		return Integer.parseInt(t.get("id"));
	}
	
	public int adicionarTurma(Turma t) throws Exception {
		turmas.add(t);		
		return Integer.parseInt(t.get("id"));
	}
				
	public boolean adicionarAtributoTurma(String nome, String tipo) throws Exception {
		if (turmas.size() > 0) {
			turmas.get(0).addAtributo(nome, tipo);
			for (int i = 1; i < turmas.size(); i ++) {
				turmas.get(i).addLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	public boolean removerAtributoTurma(String nome) throws Exception {
		if (turmas.size() > 0) {
			turmas.get(0).deleteAtributo(nome);
			for (int i = 1; i < turmas.size(); i ++) {
				turmas.get(i).deleteLocalAtributo(nome);
			}
			return true;
		}
		return false;
	}
	
	
	
	public boolean editarTurma(String campo, String valor, String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < turmas.size(); i ++) {
			if(Integer.parseInt(turmas.get(i).get("id")) == id ) {
				return(turmas.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean editarTurma(String campo, String valor, int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < turmas.size(); i ++) {
			if(Integer.parseInt(turmas.get(i).get("id")) == id ) {
				return(turmas.get(i).update(campo, valor));
			}
		}
		return false;
	}
	
	public boolean removerTurma(int codigo) throws Exception {
		int id = codigo;
		for (int i = 0; i < turmas.size(); i ++) {
			if(Integer.parseInt(turmas.get(i).get("id")) == id ) {
				turmas.get(i).delete();
				turmas.remove(turmas.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean removerTurma(String codigo) throws Exception {
		int id = Integer.parseInt(codigo);
		for (int i = 0; i < turmas.size(); i ++) {
			if(Integer.parseInt(turmas.get(i).get("id")) == id ) {
				return(turmas.get(i).delete());
			}
		}
		return false;
	}
	
	public int getSize() {
		return turmas.size();
	}
	
	public ArrayList<Turma> getTurmas() {
		return turmas;
	}
	
	public void setTurmas(ArrayList<Turma> turma) {
		this.turmas = turma;
	}
	
	public Turma getTurma(String value) {
		for (int i = 0; i < this.turmas.size(); i++) {
			if (turmas.get(i).contains(value)) {
				return turmas.get(i);
			}	
		}
		return null;
	}
	
	

	@Override
	public String toString() {
		return "ListaTurmas [turmas=" + turmas + "]";
	}
	
	public Turma get(int i) {
		return turmas.get(i);
	}
	
	
		
	
}
