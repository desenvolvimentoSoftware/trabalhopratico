package persistencia;

public class ClasseMedio extends ClasseBasico {
	public ClasseMedio(int nivel_ensino) throws Exception {
		super(nivel_ensino);
	}
	
	public ClasseMedio(int nivelEnsino, String nome, String ano_atual) throws Exception {
        super(nivelEnsino, nome, ano_atual);    
    }
    
    public ClasseMedio(int nivelEnsino, String nome, String ano_atual, int id) {
    	super(nivelEnsino, nome, ano_atual, id);   
    }
}
