package persistencia;

import java.util.HashMap;

public class DisciplinaSup extends Disciplina{
	public DisciplinaSup(int nivelEnsino) throws Exception {
		super();
        this.values = new HashMap<String, String>();
        this.add("nome", "nome");   
        this.add("ch_teorica", "4");
        this.add("ch_pratica", "4");
        this.add("ch_total", "4");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());
    }
    
    public DisciplinaSup(int nivelEnsino, String nome, int ch_teorica, int ch_pratica, int ch_total) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("ch_teorica", ""+ch_teorica);
        this.add("ch_pratica", ""+ch_pratica);
        this.add("ch_total", ""+ch_total);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public DisciplinaSup(int nivelEnsino, String nome, int ch_teorica, int ch_pratica, int ch_total, int id) {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("ch_teorica", ""+ch_teorica);
        this.add("ch_pratica", ""+ch_pratica);
        this.add("ch_total", ""+ch_total);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
}
