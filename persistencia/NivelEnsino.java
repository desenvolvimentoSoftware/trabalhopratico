package persistencia;


public interface NivelEnsino {
	public boolean prepareDatabase() throws Exception;
	public boolean distribuir();
	public boolean recuperaDatabase() throws Exception;
	
}
