package persistencia;

import java.util.HashMap;

public class ClasseTec extends Classe{
	public ClasseTec(int nivelEnsino) throws Exception {
		super();
        this.values = new HashMap<String, String>();
        this.add("nome", "nome");   
        this.add("semestre", "4");
        this.add("data_inicio", "4");
        this.add("data_fim", "4");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());
    }
    
    public ClasseTec(int nivelEnsino, String nome, int semestre, String dataInicio, String dataFim) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("semestre", ""+semestre);
        this.add("data_inicio", "dataInicio");
        this.add("data_fim", "dataFim");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public ClasseTec(int nivelEnsino, String nome, int semestre, String dataInicio, String dataFim, int id) {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("semestre", ""+semestre);
        this.add("data_inicio", "dataInicio");
        this.add("data_fim", "dataFim");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
}
