package persistencia;

import java.util.HashMap;

public class ClasseBasico extends Classe{
	public ClasseBasico(int nivelEnsino) throws Exception {
		super();
        this.values = new HashMap<String, String>();
        this.add("nome", "nome");   
        this.add("ano_atual", "200");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());
    }
    
    public ClasseBasico(int nivelEnsino, String nome, String ano_atual) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("ano_atual", ano_atual);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public ClasseBasico(int nivelEnsino, String nome, String ano_atual, int id) {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("ano_atual", ano_atual);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
}
