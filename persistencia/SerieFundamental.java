package persistencia;

import java.util.HashMap;

public class SerieFundamental extends Serie{
	public SerieFundamental(int nivelEnsino) throws Exception {
		super();
        this.values = new HashMap<String, String>();
        this.add("nome", "nome");   
        this.add("descricao", "descircao");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());
    }
    
    public SerieFundamental(int nivelEnsino, String nome, String descricao) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("descricao", "descricao");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public SerieFundamental(int nivelEnsino, String nome, String descricao, int id) {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("descricao", "descricao");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
}
