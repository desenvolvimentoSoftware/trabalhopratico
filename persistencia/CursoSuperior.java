package persistencia;

import java.util.HashMap;

public class CursoSuperior extends Curso{
	
	public CursoSuperior(int nivelEnsino) throws Exception {
		super();
        this.values = new HashMap<String, String>();
        this.add("nome", "nome");
        this.add("acronimo", "ac");
        this.add("descricao", "descricao");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());
    }
    
    public CursoSuperior(int nivelEnsino, String nome, String acronimo, String descricao) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("acronimo", acronimo);
        this.add("descricao", descricao);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public CursoSuperior(int nivelEnsino, String nome, String acronimo, String descricao, int id) {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("acronimo", acronimo);
        this.add("descricao", descricao);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
    
}
