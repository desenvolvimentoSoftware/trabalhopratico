package persistencia;

import java.util.HashMap;

public class DisciplinaSupTurma {
private HashMap<String, String> values;
    
    public DisciplinaSupTurma(int nivelEnsino, int disciplina, int turma) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("disciplina", ""+disciplina);
        this.add("turma", ""+turma);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public DisciplinaSupTurma(int nivelEnsino, int disciplina, int turma, int id) {
        this.values = new HashMap<String, String>();
        this.add("disciplina", ""+disciplina);
        this.add("turma", ""+turma);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
    
    public void add(String name, String value) {
    	values.put(name, value);
    	return;
    }
    
    public boolean addAtributo(String name, String tipo) throws Exception {
    	if (!values.containsKey(name)) {
    		values.put(name, "null");
    		String sql = "ALTER TABLE disciplinas_turmas ADD "+ name +" " + tipo + ";";
    		if(Conexao.alterarTabela(sql)) {
    			return true;
    		} else {
    			return false;
    		}
    	}
    	return true;
    }
    
    public boolean addLocalAtributo (String name) throws Exception {
    	if (!values.containsKey(name)) 
    		values.put(name, "null");
    	return true;
    }
    
    public boolean deleteAtributo(String name) throws Exception {
    	if (values.containsKey(name)) {
    		values.remove(name);
    		String sql = "ALTER TABLE disciplinas_turmas DROP COLUMN "+ name +";";
    		if(Conexao.alterarTabela(sql)) {
    			return true;
    		} else {
    			return false;
    		}
    	}
    	return true;
    }
    
    public boolean deleteLocalAtributo (String name) throws Exception {
    	if (values.containsKey(name)) 
    		values.remove(name);

    	return true;
    }
    
    public boolean delete() throws Exception {
    	String sql = "DELETE FROM disciplinas_turmas WHERE id = "+ this.get("id"); 
    	return Conexao.delete(sql);
    }
    
    
    public String get(String name)
    {
        return values.get(name);
    }
    
    public boolean update(String name, String value) throws Exception
    {
    	if (values.containsKey(name)) {
    		if (this.editarDatabase(name, value)) {
    			values.put(name, value);
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		return false;
    	}
 
    }
    
    public boolean editarDatabase(String name, String value) throws Exception {
    	String sql = "UPDATE disciplinas_turmas SET "+name+" = '"+value+"' WHERE id = "+this.get("id");
    	System.out.println(sql);
    	Conexao.update(sql);
    	return false;
    }
    
    public int inserirDatabase() throws Exception {
    	String sql = "INSERT INTO disciplinas_turmas (";
    	
    	boolean flag = false;
    	for (String key : values.keySet()) {
    		if (flag) sql += ",";
    	    sql += key;
    	    flag = true;
    	}
    	
    	sql+= ") VALUES(";
    	
    	flag = false;
    	for (String value : values.values()) {
    		if (flag) sql += ",";
    	    sql += "'" + value + "'";
    	    flag = true;
    	}
    	
    	sql += ")";
    	
    	System.out.println(sql);
    	return Conexao.post(sql);
    }
    
    public boolean contains(String value) {
    	return values.containsKey(value);
    }

	@Override
	public String toString() {
		return "Disciplinas_Turmas [values=" + values + "]";
	}
}
