package persistencia;

import java.util.HashMap;

public class DisciplinaMedio extends Disciplina{
	public DisciplinaMedio(int nivelEnsino) throws Exception {
		super();
        this.values = new HashMap<String, String>();
        this.add("nome", "nome");   
        this.add("ch_total", "4");
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());
    }
    
    public DisciplinaMedio(int nivelEnsino, String nome, int ch_total) throws Exception {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("ch_total", ""+ch_total);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+this.inserirDatabase());        
    }
    
    public DisciplinaMedio(int nivelEnsino, String nome, int ch_total, int id) {
        this.values = new HashMap<String, String>();
        this.add("nome", nome);
        this.add("ch_total", ""+ch_total);
        this.add("nivel_ensino_id", ""+nivelEnsino);
        this.add("id", ""+ id);   
    }
}
