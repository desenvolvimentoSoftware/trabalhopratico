package persistencia;

import java.sql.ResultSet;
import java.util.ArrayList;

import Negocio.ListaCursos;
import Negocio.ListaDisciplinaSup;
import Negocio.ListaDisciplinaSupTurma;
import Negocio.ListaPesosTurmas;
import Negocio.ListaProfessores;
import Negocio.ListaTurmas;

public abstract class SuperiorPersistencia implements NivelEnsino{
	protected int id;
	protected ListaDisciplinaSup disciplinas;
	protected ListaCursos cursos;
	protected ListaTurmas turmas;
	protected ListaProfessores professores;
	protected ListaPesosTurmas pesos;
	protected ArrayList<CursoSuperiorTurma> cursos_turmas;
	protected ListaDisciplinaSupTurma disciplinas_turmas;
	/**
	 * @throws Exception 
	 * @method prepareDatabase
	 * @description Este método é responsável por criar as tabelas necessárias
	 * no banco de dados para que o nível de ensino possa própriamente operar.
	 * São criadas as tabelas: Turma, Curso, Disciplina e Professor  
	 */
	@Override	
	public boolean prepareDatabase() throws Exception {
		//Criar Turma
		Conexao.createTable("CREATE TABLE IF NOT EXISTS turmas(id int NOT NULL AUTO_INCREMENT, nivel_ensino_id int, nome varchar(255), semestre int, PRIMARY KEY(id) )");
		
		//Criar Curso
		Conexao.createTable("CREATE TABLE IF NOT EXISTS cursos(id int NOT NULL AUTO_INCREMENT, nivel_ensino_id int, nome varchar(255), acronimo varchar(255), descricao varchar(255), PRIMARY KEY(id) )");
		
		//Criar CursosTurmas
		Conexao.createTable("CREATE TABLE IF NOT EXISTS cursos_turmas(id int NOT NULL AUTO_INCREMENT, nivel_ensino_id int, curso_id int, turma_id int, PRIMARY KEY(id) )");
		
		//Criar Disciplina
		Conexao.createTable("CREATE TABLE IF NOT EXISTS disciplinas(id int NOT NULL AUTO_INCREMENT, nivel_ensino_id int, nome varchar(255), ch_total int, usa_laboratorio int, ch_pratica int, ch_teorica int, PRIMARY KEY(id) )");
		
		//Criar DisciplinasTurmas                      
		Conexao.createTable("CREATE TABLE IF NOT EXISTS disciplinas_turmas(id int NOT NULL AUTO_INCREMENT, nivel_ensino_id int, disciplina_id int, turma_id int, PRIMARY KEY(id) )");
		
		//Criar Professor
		Conexao.createTable("CREATE TABLE IF NOT EXISTS professores(id int NOT NULL AUTO_INCREMENT, nivel_ensino_id int, nome varchar(255), email varchar(255), ch_minima int, ch_maxima int, PRIMARY KEY(id) )");
		
		//Criar Professor
		Conexao.createTable("CREATE TABLE IF NOT EXISTS pesos_turmas(id int NOT NULL AUTO_INCREMENT,  nivel_ensino_id int, professor_id int, peso_id int, turma_id int, PRIMARY KEY(id) )");
		
		
		return false;
	}
	
	@Override
	public boolean recuperaDatabase() throws Exception {
		//Recupera Turma
		ResultSet result = Conexao.get("SELECT * FROM turmas WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			Turma t = new Turma(this.id, result.getString("nome"), Integer.parseInt(result.getString("semestre")), Integer.parseInt(result.getString("id")) );
			turmas.adicionarTurma(t);
		}
		
		//Recuperar Curso
		result = Conexao.get("SELECT * FROM cursos WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			CursoSuperior c = new CursoSuperior(this.id, result.getString("nome"), result.getString("acronimo"), result.getString("descricao"), Integer.parseInt(result.getString("id")) );
			cursos.adicionarCurso(c);
		}
		
		//Recuperar CursosTurmas
		result = Conexao.get("SELECT * FROM cursos_turmas WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			CursoSuperiorTurma ct = new CursoSuperiorTurma(this.id, Integer.parseInt(result.getString("curso_id")), Integer.parseInt(result.getString("turma_id")), Integer.parseInt(result.getString("id")) );
			cursos_turmas.add(ct);
		}
		
		//Recuperar Disciplina
		result = Conexao.get("SELECT * FROM disciplinas WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			DisciplinaSup d = new DisciplinaSup(this.id, result.getString("nome"), Integer.parseInt(result.getString("ch_teorica")), Integer.parseInt(result.getString("ch_pratica")), Integer.parseInt(result.getString("ch_total")), Integer.parseInt(result.getString("id")) );
			disciplinas.adicionarDisciplina(d);
		}
		
		//Recuperar DisciplinasTurmas
		result = Conexao.get("SELECT * FROM disciplinas_turmas WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			DisciplinaSupTurma dt = new DisciplinaSupTurma(this.id, Integer.parseInt(result.getString("disciplina_id")), Integer.parseInt(result.getString("turma_id")), Integer.parseInt(result.getString("id")) );
			disciplinas_turmas.adicionarDisciplinaTurma(dt);
		}

		//Recuperar Professor
		result = Conexao.get("SELECT * FROM professores WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			Professor p = new Professor(this.id, result.getString("nome"), result.getString("email"), Integer.parseInt(result.getString("ch_minima")), Integer.parseInt(result.getString("ch_maxima")),Integer.parseInt(result.getString("id")) );
			professores.adicionarProfessor(p);
		}
		
		//Recuperar PesoTurmas
		result = Conexao.get("SELECT * FROM pesos_turmas WHERE nivel_ensino_id = " + id);
		while (result.next()) {
			PesoTurma pt = new PesoTurma(this.id, Integer.parseInt(result.getString("professor_id")), Integer.parseInt(result.getString("peso")), Integer.parseInt(result.getString("turma_id")), Integer.parseInt(result.getString("id")) );
			pesos.adicionarPesoTurma(pt);
		}
		
		
		return false;
	}
	
	public int getDisciplinasSize() {
		return disciplinas.getSize();
	}
	
	public int getCursosSize() {
		return cursos.getSize();
	}
	
	public int getProfessoresSize() {
		return professores.getSize();
	}
	
	public int getTurmasSize() {
		return turmas.getSize();
	}
	
	@Override
	public String toString() {
		return "Superior [id=" + id + ", disciplinas=" + disciplinas + ", cursos=" + cursos + ", turmas=" + turmas
				+ ", professores=" + professores + ", pesos=" + pesos + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<DisciplinaSup> getDisciplinas() {
		return disciplinas.getDisciplinas();
	}

	public void setDisciplinas(ArrayList<DisciplinaSup> disciplinas) {
		this.disciplinas.setDisciplinas(disciplinas);
	}

	public ArrayList<CursoSuperior> getCursos() {
		return cursos.getCursos();
	}

	public void setCursos(ArrayList<CursoSuperior> cursos) {
		this.cursos.setCursos(cursos);
	}

	public ArrayList<Turma> getTurmas() {
		return turmas.getTurmas();
	}

	public void setTurmas(ArrayList<Turma> turmas) {
		this.turmas.setTurmas(turmas);
	}

	public ArrayList<Professor> getProfessores() {
		return professores.getProfessores();
	}

	public void setProfessores(ArrayList<Professor> professores) {
		this.professores.setProfessores(professores);
	}

	public ArrayList<PesoTurma> getPesos() {
		return pesos.getPesos();
	}

	public void setPesos(ArrayList<PesoTurma> pesos) {
		this.pesos.setPesos(pesos);
	}
	
}
