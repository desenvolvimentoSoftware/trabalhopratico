package persistencia;


public class ClasseFundamental extends ClasseBasico{
	public ClasseFundamental(int nivel_ensino) throws Exception {
		super(nivel_ensino);
	}
	
	public ClasseFundamental(int nivelEnsino, String nome, String ano_atual) throws Exception {
        super(nivelEnsino, nome, ano_atual);    
    }
    
    public ClasseFundamental(int nivelEnsino, String nome, String ano_atual, int id) {
    	super(nivelEnsino, nome, ano_atual, id);   
    }
}
