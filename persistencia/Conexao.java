package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class Conexao {
	
	Connection con;
	
	public Conexao () {
		
	}
	
	public static Connection getConnection() throws Exception{
		  try{
		   String driver = "com.mysql.jdbc.Driver";
		   String url = "jdbc:mysql://localhost:3306/distribuicao";
		   String username = "root";
		   String password = "xqQ1*f/LaK=y";
		   Class.forName(driver);
		   
		   Connection conn = DriverManager.getConnection(url,username,password);
		   return conn;
		  } catch(Exception e){
			  System.out.println(e);
		  }
		  	  
		  return null;
	 }
	
	public static void createTable(String sql) throws Exception{
		try {
			Connection con = getConnection();
			PreparedStatement create = con.prepareStatement(sql);
			create.executeUpdate();			
		} catch(Exception e){
			System.out.println(e);
		} 
		return;
	}
	
	public static int post(String sql) throws Exception {
		int id = -1;
		try {
			Connection con = getConnection();
			PreparedStatement posted = con.prepareStatement(sql,  Statement.RETURN_GENERATED_KEYS);
			id = posted.executeUpdate();
			
			ResultSet rs = posted.getGeneratedKeys();
			if (rs.next()){
			    id=rs.getInt(1);
			}
		} catch(Exception e){
			System.out.println(e);
		} 
		return id; 
	}
	
	public static boolean alterarTabela(String sql) throws Exception {
		boolean flag = false;
		try {
			Connection con = getConnection();
			PreparedStatement posted = con.prepareStatement(sql);
			posted.executeUpdate();
			flag = true;
			
		} catch(Exception e){
			System.out.println(e);
		} 
		return flag;
	}
	
	public static boolean update(String sql) throws Exception {
		boolean flag = false;
		
		try {
			Connection con = getConnection();
			PreparedStatement posted = con.prepareStatement(sql);
			posted.executeUpdate();
			flag = true;
		} catch(Exception e){
			System.out.println(e);
		} 
		return flag; 
	}
	
	public static boolean delete(String sql) throws Exception {
		boolean flag = false;
		
		try {
			Connection con = getConnection();
			PreparedStatement posted = con.prepareStatement(sql);
			posted.executeUpdate();
			flag = true;
		} catch(Exception e){
			System.out.println(e);
		} 
		return flag; 
	}
	
	public static ResultSet get(String sql) throws Exception{
		ResultSet result;
		try {
			Connection con = getConnection();
			PreparedStatement statement = con.prepareStatement(sql);
			result = statement.executeQuery();	
			return result;
		}catch(Exception e){
			System.out.println(e);
		} 
		return null;
	}
	
	public static int criarNivelEnsino(String nivelEnsino) throws Exception {
		int id = -1;
		
		createTable("CREATE TABLE IF NOT EXISTS nivel_ensino(id int NOT NULL AUTO_INCREMENT, nivel varchar(255), PRIMARY KEY(id) )");
		
		try {
			Connection con = getConnection();
			
			PreparedStatement statement = con.prepareStatement("SELECT * FROM nivel_ensino WHERE nivel =  '" + nivelEnsino + "';",  Statement.RETURN_GENERATED_KEYS);
			ResultSet result = statement.executeQuery();
			
			int count = 0;
			while (result.next()) {
				count += 1;
				id = Integer.parseInt(result.getString("id"));
			}
			
			if (count == 0) {
				statement = con.prepareStatement("INSERT INTO nivel_ensino (nivel) VALUES ('"+nivelEnsino+"')", Statement.RETURN_GENERATED_KEYS);
				id = statement.executeUpdate();
				
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()){
				    id=rs.getInt(1);
				}
			}
		} catch(Exception e){
			System.out.println(e);
		} 
		return id;
	}
	
} 
