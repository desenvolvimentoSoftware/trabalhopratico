package Interface;
import Negocio.Superior;
import persistencia.Conexao;

public class SistemaDistribuicao {

	public static void main(String[] args) throws Exception {
		Conexao.getConnection();
		System.out.println("Criando Nivel de Ensino Superior");
		Superior sup = new Superior();
		System.out.println("Nivel Criado");
		
		System.out.println("Quantidade de Professores: (3)");
		System.out.println("1 nome:Alessandro email:ale@email.com chmin:4 chmax:8");
		int idp1 = sup.adicionarProfessor("Alessandro", "ale@email.com", 4, 8);
		
		System.out.println("1 nome:Bruna email:b@email.com chmin:4 chmax:8");
		int idp2 = sup.adicionarProfessor("Bruna", "b@email.com", 4, 8);
		
		System.out.println("1 nome:Marcio email:b@email.com chmin:4 chmax:8");
		int idp3 = sup.adicionarProfessor("Marcio", "m@email.com", 4, 8);
		
		System.out.println("Quantidade de Turmas: (2)");
		System.out.println("Prog T1 semestre:1");
		int idDisc = sup.adicionarDisciplina("Prog", 4, 0, 96);
		int idTurma1 = sup.adicionarTurma("T1", 1);
		
		System.out.println("Prog T2 semestre:1");
		int idTurma2 = sup.adicionarTurma("T1", 1);
		
		System.out.println("Pesos dos Professores:");
		System.out.println("Bruna 1 T1 Prog:");
		sup.adicionarPesoTurma(idp2, 1, idTurma1);
		System.out.println("Bruna 10 T2 Prog:");
		sup.adicionarPesoTurma(idp2, 10, idTurma2);

		System.out.println("Marcio 10 T2 Prog:");
		sup.adicionarPesoTurma(idp3, 10, idTurma2);

		System.out.println("Alessandro 10 T1 Prog:");
		sup.adicionarPesoTurma(idp1, 10, idTurma1);
		
		System.out.println("Sistema em Detalhes:");
		System.out.println(sup);
		
		System.out.println("Criando a distribuicao");
		sup.distribuir();
	}
		 

}
